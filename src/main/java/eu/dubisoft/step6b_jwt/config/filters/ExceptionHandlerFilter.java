package eu.dubisoft.step6b_jwt.config.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;


@Component
@Slf4j
public class ExceptionHandlerFilter extends OncePerRequestFilter {

  // INFO: tutaj musimy uzyc wstrzykniecia z kwalifikatorem, poniewaz dla HandlerExceptionResolver istnieja 2 dostepne implementacje
  @Autowired
  @Qualifier("handlerExceptionResolver")
  private HandlerExceptionResolver resolver;

  // INFO: to jest lekka wariacja na temat przechwytywania wyjatkow, takie podejscie pozwala nam uzywac tego samego RequestExceptionHandler'a, ktorego uzywamy dla reszty aplikacji
  @Override
  public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    try {
      filterChain.doFilter(request, response);
    } catch (RuntimeException e) {
      resolver.resolveException(request, response, null, e);
    }
  }
}