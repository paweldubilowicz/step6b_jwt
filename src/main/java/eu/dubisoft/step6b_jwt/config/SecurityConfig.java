package eu.dubisoft.step6b_jwt.config;

import eu.dubisoft.step6b_jwt.config.filters.AuthorizationFilter;
import eu.dubisoft.step6b_jwt.config.filters.ExceptionHandlerFilter;
import eu.dubisoft.step6b_jwt.config.filters.SimpleCorsFilter;
import eu.dubisoft.step6b_jwt.config.handlers.RestAccessDeniedHandler;
import eu.dubisoft.step6b_jwt.config.model.Permission;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final RestAccessDeniedHandler accessDeniedHandler;
  private final UserDetailsService jwtUserDetailsService;
  private final AuthorizationFilter authorizationFilter;
  private final SimpleCorsFilter simpleCorsFilter;
  private final ExceptionHandlerFilter exceptionHandlerFilter;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(jwtUserDetailsService)
        .passwordEncoder(passwordEncoder());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()

        .headers()
          .cacheControl().and()
          .frameOptions().deny()
          .xssProtection().xssProtectionEnabled(true).and()
          .contentTypeOptions().and()
        .and()
        .csrf().disable()

        // INFO: Tutaj dodajemy 3 filtry i wskazujemy ich kolejnosc. Wywolane beda w nastepujacej kolejnosci:
        // ExceptionHandlerFilter - opakowuje dalsze filtry w try/catch (@ControllerAdvice lapie bledy dopiero z Controllerow)
        // SimpleCorsFilter - obsluga CORSa
        // AuthorizationFilter - filtr odpowiadajacy za utworzenie sesji dla tokena na czas requesta
        .addFilterBefore(authorizationFilter, UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(simpleCorsFilter, AuthorizationFilter.class)
        .addFilterBefore(exceptionHandlerFilter, SimpleCorsFilter.class)

        // INFO: tutaj mamy alternatywne podejscie do uzywania @PreAuthorize, zalety sa 3:
        // - mamy informacje w jednym centralnym punkcie
        // - mozemy generalizowac sciezki uzywajac pathPatternow
        // - mozemy uzyc slownika rol
        // Ponizsza konfiguracja odwzorowuje to co bylo w step6a_basic
        .authorizeRequests()
        .antMatchers("/test/adminService").hasAnyAuthority(Permission.ADMIN.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/userService").hasAnyAuthority(Permission.USER.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/commonService").hasAnyAuthority(Permission.USER.name(), Permission.ADMIN.name(), Permission.FULL_ACCESS.name())
        .antMatchers("/test/authenticatedService").authenticated()
        .antMatchers("/test/openService").permitAll()
        // INFO: Pozostale sa z defaulta ustawione na PERMIT ALL

        // INFO: tutaj dodajemy obsluge bledow zwiazanych z niewlasciwymi uprawnieniami
        // ExceptionHandlerFilter nie jest w stanie tego przechwycic i obsluzyc, wiec potrzebny jest oddzielny sposob
        .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
    ;
  }
}
