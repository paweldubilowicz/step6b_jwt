package eu.dubisoft.step6b_jwt.api;

import eu.dubisoft.step6b_jwt.model.AuthData;
import eu.dubisoft.step6b_jwt.model.JwtResponse;
import eu.dubisoft.step6b_jwt.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

  private final AuthService authService;

  @PostMapping(value = "/login")
  public ResponseEntity<JwtResponse> login(@RequestBody AuthData authenticationRequest) throws AuthenticationException {
    return ResponseEntity.ok(authService.loginAndGenerateToken(authenticationRequest.getLogin(), authenticationRequest.getPassword()));
  }
}