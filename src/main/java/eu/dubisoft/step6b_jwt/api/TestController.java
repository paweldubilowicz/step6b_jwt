package eu.dubisoft.step6b_jwt.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

  @GetMapping({"/adminService"})
  public String adminService() {
    return "adminService success";
  }

  @GetMapping({"/userService"})
  public String userService() {
    return "userService success";
  }

  @GetMapping({"/commonService"})
  public String commonService() {
    return "commonService success";
  }

  @GetMapping({"/authenticatedService"})
  public String authenticatedService() {
    return "authenticatedService success";
  }

  @GetMapping({"/openService"})
  public String openService() {
    return "openService success";
  }

  @GetMapping({"/unexpectedService"})
  public String unexpectedService() {
    return "unexpectedService success";
  }
}