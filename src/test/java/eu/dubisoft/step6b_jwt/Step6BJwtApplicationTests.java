package eu.dubisoft.step6b_jwt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Step6BJwtApplicationTests {

  @Test
  public void contextLoads() {
  }

}
