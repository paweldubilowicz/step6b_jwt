package eu.dubisoft.step6b_jwt.repository;

import eu.dubisoft.step6b_jwt.config.model.JwtUserDetails;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserMemoryRepository implements UserRepository {

  private Map<String, JwtUserDetails> USERS = new HashMap<>();

  @Override
  public void addUser(JwtUserDetails userDetails) {
    USERS.put(userDetails.getUsername(), userDetails);
  }

  @Override
  public JwtUserDetails findByUserame(String username) {
    return USERS.get(username);
  }
}
