package eu.dubisoft.step6b_jwt;

import eu.dubisoft.step6b_jwt.config.model.JwtUserDetails;
import eu.dubisoft.step6b_jwt.repository.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostConstructService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @PostConstruct
  private void init() {
    userRepository.addUser(new JwtUserDetails(Arrays.asList(new SimpleGrantedAuthority("ADMIN")), passwordEncoder.encode("Password1"), "User1", true, true, true, true));
    userRepository.addUser(new JwtUserDetails(Arrays.asList(new SimpleGrantedAuthority("USER")), passwordEncoder.encode("Password2"), "User2", true, true, true, true));
    userRepository.addUser(new JwtUserDetails(Arrays.asList(new SimpleGrantedAuthority("ADMIN"), new SimpleGrantedAuthority("USER")), passwordEncoder.encode("Password3"), "User3", true, true, true, true));
    userRepository.addUser(new JwtUserDetails(Arrays.asList(new SimpleGrantedAuthority("FULL_ACCESS")), passwordEncoder.encode("Password4"), "User4", true, true, true, true));
    userRepository.addUser(new JwtUserDetails(new ArrayList<>(), passwordEncoder.encode("Password5"), "User5", true, true, true, true));
  }
}
