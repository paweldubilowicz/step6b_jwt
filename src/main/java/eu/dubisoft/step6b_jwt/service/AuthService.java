package eu.dubisoft.step6b_jwt.service;

import eu.dubisoft.step6b_jwt.model.JwtResponse;
import eu.dubisoft.step6b_jwt.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService {

  private final AuthenticationManager authenticationManager;
  private final JwtTokenUtil jwtTokenUtil;

  public JwtResponse loginAndGenerateToken(String username, String password) throws AuthenticationException {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    return new JwtResponse(jwtTokenUtil.generateToken(username));
  }
}
