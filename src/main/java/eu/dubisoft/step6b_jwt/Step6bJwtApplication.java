package eu.dubisoft.step6b_jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Step6bJwtApplication {

  public static void main(String[] args) {
    SpringApplication.run(Step6bJwtApplication.class, args);
  }

}
