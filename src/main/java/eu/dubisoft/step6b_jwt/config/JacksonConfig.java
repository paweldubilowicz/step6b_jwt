package eu.dubisoft.step6b_jwt.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class JacksonConfig {

  // INFO: bez dodatkowej konfiguracji ObjectMappera istnialyby rozjazdy miedzy responsami z restow i RequestExceptionHandlera, a responsami z RestAccessDeniedHandler.
  // Ogolnie - wstrzykniety ObjectMapper, i ten uzywany automatycznie przy serializacji i deserializacji responsow i requestow domyslnie sa rozne
  // Dorzucajac konfiguracje wymuszamy, zeby w obu przypadkach uzywany byl ten sam i narzucamy kilka przydatnych zasad
  @Bean
  public Jackson2ObjectMapperBuilder objectMapperBuilder() {
    return new Jackson2ObjectMapperBuilder()
        .serializationInclusion(Include.ALWAYS) // wysylaj w JSONie takze pola nullowe (wygodniejszy debug, wieksze zuzycie transferu)
        .featuresToDisable(
            SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
            DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .featuresToEnable(
            DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
        .modules(new JavaTimeModule()); // obsluga Java Time
  }
}