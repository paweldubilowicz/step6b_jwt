package eu.dubisoft.step6b_jwt.config.model;

public enum Permission {
  ADMIN,
  USER,
  FULL_ACCESS;
}
