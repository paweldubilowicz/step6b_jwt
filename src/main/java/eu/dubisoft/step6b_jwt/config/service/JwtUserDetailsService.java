package eu.dubisoft.step6b_jwt.config.service;

import eu.dubisoft.step6b_jwt.config.model.JwtUserDetails;
import eu.dubisoft.step6b_jwt.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    JwtUserDetails res = userRepository.findByUserame(username);
    if (res == null) {
      throw new UsernameNotFoundException("User " + username + " not found");
    }
    return res;
  }
}
