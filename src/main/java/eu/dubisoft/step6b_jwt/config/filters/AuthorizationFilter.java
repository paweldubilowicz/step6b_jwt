package eu.dubisoft.step6b_jwt.config.filters;

import eu.dubisoft.step6b_jwt.config.service.JwtUserDetailsService;
import eu.dubisoft.step6b_jwt.utils.JwtTokenUtil;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuthorizationFilter extends OncePerRequestFilter {

  private final JwtUserDetailsService jwtUserDetailsService;
  private final JwtTokenUtil jwtTokenUtil;

  // INFO: wykluczenia z filtracji robimy po to, zeby cokolwiek mialoby wyjibac sie w filtrze w momencie niespelnienia jego wymagan nie wywolalo sie
  // np: nie chcemy odlozyc w logu/bazie informacji o niepoprawnym tokenie przy /authenticate (gdzie nie jest on wymagany)
  private final List<String> EXCLUDE_PATTERNS = Arrays.asList("/auth/login", "/test/openService");
  private final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
    return EXCLUDE_PATTERNS.stream()
        .anyMatch(p -> PATH_MATCHER.match(p, request.getServletPath()));
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
    authorize(request, response);
    chain.doFilter(request, response);
  }

  private void authorize(HttpServletRequest request, HttpServletResponse response) {
    String token = extractToken(request);
    String username = extractUsername(token);

    UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
    validateUserDetails(userDetails);
    jwtTokenUtil.validateToken(token, userDetails);
    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities()));
  }

  private String extractToken(HttpServletRequest request) {
    String token = request.getHeader("Authorization");
    if (token == null) {
      throw new SessionAuthenticationException("Missing token");
    }
    // INFO: mozna smialo pominac nastepne 4 linijki i zastapic je zwyklym return token; - nie bedzie wymagany wowczas prefiks Bearer do tokena
    if (!token.startsWith("Bearer ")) {
      throw new SessionAuthenticationException("Invalid token");
    }
    return token.substring(7);
  }

  private String extractUsername(String token) throws SessionAuthenticationException {
    return jwtTokenUtil.getUsernameFromToken(token);
  }

  private void validateUserDetails(UserDetails userDetails) {
    if (!userDetails.isEnabled()) {
      throw new SessionAuthenticationException("Account disabled");
    }
    if (!userDetails.isAccountNonLocked()) {
      throw new SessionAuthenticationException("Account locked");

    }
    if (!userDetails.isAccountNonExpired()) {
      throw new SessionAuthenticationException("Account expired");

    }
  }
}