package eu.dubisoft.step6b_jwt.repository;

import eu.dubisoft.step6b_jwt.config.model.JwtUserDetails;

public interface UserRepository {

  void addUser(JwtUserDetails userDetails);

  JwtUserDetails findByUserame(String username);
}
