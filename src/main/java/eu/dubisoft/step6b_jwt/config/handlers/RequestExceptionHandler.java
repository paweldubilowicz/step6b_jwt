package eu.dubisoft.step6b_jwt.config.handlers;

import eu.dubisoft.step6b_jwt.model.ErrorResponse;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RequestExceptionHandler {

    // INFO: Tutaj trafiaja wyjatki nieobsluzone w trakcie pracy aplikacji
    // Pomimo istnienia innych sposobow na tworzenie customowych responsow, sensowniejszym wydaje sie jeden punkt centralny.
    // Zwykle mamy binarne wyniki zapytania - albo wyszlo (kody z serii 2xx) albo nie wyszlo (4xx lub 5xx)
    // Klient dostaje zwrotke z kodem bledu, ewentualnie z jakims komunikatem z frameworka, ktory nie zawsze bedzie trafny
    // Przyklad bledu 400 (BAD_REQUEST) moze oznaczac milion rzeczy - brak jakiegos pola, niedopuszczana wartosc w polu, zly typ danych, etc.
    // Chcac wyswietlic uzytkownikowi informacje co poszlo nie tak - nie mamy takiej mozliwosci, dlatego warto zainwestowac w customowe response'y
    // W takim przypadku jak wszystko pojdzie OK to zwracamy normalny response i jest on dalej obslugiwany, jak nie, dostajemy kompletnie inny response,
    // ktorego obsluge musimy sami zrobic i np poinformowac odpowiednio uzytkownika.
    // Nastepny przyklad bedzie zawierac taka obsluge i pokaze minimalnie bardziej zaawansowane podejscie do propagacji bledow.


    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> handle(final BadCredentialsException e) {
        return new ResponseEntity(new ErrorResponse(400, "INVALID_CREDENTIALS", "Invalid login and/or password", new Date()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handle(final IllegalArgumentException e) {
        return new ResponseEntity(new ErrorResponse(400, "INVALID_INPUT", e.getMessage(), new Date()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SessionAuthenticationException.class)
    public ResponseEntity<ErrorResponse> handle(final SessionAuthenticationException e) {
        return new ResponseEntity(new ErrorResponse(401, "UNAUTHORIZED", e.getMessage(), new Date()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handle(final Exception e) {
        // INFO: warto po nieoczekiwanych problemach zostawiac jakis slad, ale nie informowac uzytkownika co nie dziala na serwerze
        log.error("Unknown error", e);
        return new ResponseEntity(new ErrorResponse(500, "UNKNOWN_ERROR", "Unknown error", new Date()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
