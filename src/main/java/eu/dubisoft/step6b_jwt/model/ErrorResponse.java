package eu.dubisoft.step6b_jwt.model;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ErrorResponse implements Serializable {

  private int status;
  private String internalCode;
  private String message;
  private Date timestamp;
}
