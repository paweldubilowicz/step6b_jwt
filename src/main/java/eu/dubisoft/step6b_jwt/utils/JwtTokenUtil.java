package eu.dubisoft.step6b_jwt.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenUtil implements Serializable {

  @Value("${jwt.token_lifetime_in_minutes}")
  private int tokenLifetimeInMinutes;

  @Value("${jwt.secret}")
  private String secret;

  public String generateToken(String username) {
    Map<String, Object> claims = new HashMap<>();
    return doGenerateToken(claims, username);
  }

  public String getUsernameFromToken(String token) throws SessionAuthenticationException {
    return getClaimFromToken(token, Claims::getSubject);
  }

  public void validateToken(String token, UserDetails userDetails) {
    if (!getUsernameFromToken(token).equals(userDetails.getUsername())) {
      throw new SessionAuthenticationException("Invalid token");
    }
    if (isTokenExpired(token)) {
      throw new SessionAuthenticationException("Expired token");
    }
  }

  private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) throws SessionAuthenticationException {
    try {
      final Claims claims = getAllClaimsFromToken(token);
      return claimsResolver.apply(claims);
    } catch (ExpiredJwtException ex) {
      throw new SessionAuthenticationException("Expired token");
    } catch (Exception ex) {
      throw new SessionAuthenticationException("Invalid token");
    }
  }

  private String doGenerateToken(Map<String, Object> claims, String username) {
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(username)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + tokenLifetimeInMinutes * 60 * 1000))
        .signWith(SignatureAlgorithm.HS512, secret)
        .compact();
  }

  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  private Date getExpirationDateFromToken(String token) throws AccessDeniedException {
    return getClaimFromToken(token, Claims::getExpiration);
  }
}